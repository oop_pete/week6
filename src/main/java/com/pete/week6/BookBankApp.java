package com.pete.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank pete = new BookBank("pete", 100.0);
        pete.name = "pete";
        pete.balance = 100.0;
        pete.print();
        pete.deposit(50);
        pete.print();
        pete.withdraw(50);
        pete.print();

        BookBank prayood = new BookBank("Prayood", 1);
        prayood.name = "Prayood";
        prayood.balance = 1;
        prayood.deposit(1000000);
        prayood.withdraw(10000000);
        prayood.print();

        BookBank praweeet = new BookBank("Praweeet", 10);
        praweeet.name = "Praweeet";
        praweeet.balance = 10;
        praweeet.deposit(10000000);
        praweeet.withdraw(1000000);
        praweeet.print();
    }
}
